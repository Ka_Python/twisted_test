from twisted.web.server import Site
from twisted.internet import reactor
from twisted.web.resource import Resource

f = open('template.html')
st = str(f.read())
f.close()


class Server(Resource):
    isLeaf = True

    def getChild(self, path, request):
        if path == '':
            return self
        return Resource.getChild(self, path, request)

    def render_GET(self, request):
        tmp_0 = "I'm stay %r" % (request.prepath,)
        return tmp_0


class FormPage(Resource):
    def getChild(self, path, request):
        if path == '':
            return self
        return Resource.getChild(self, path, request)

    def render_GET(self, request):
        return st

root = Resource()
root.putChild("form", FormPage())
root.putChild('add', Server())
factory = Site(root)

reactor.listenTCP(8080, factory)
reactor.run()


