__author__ = 'ka_python'
from twisted.internet import reactor, protocol
from twisted.protocols import basic


class MyProtocol(basic.LineReceiver):
    def connectionMade(self):
        self.factory.clients.append(self)

    def lineReceived(self, data):
        #self.transport.write('%s\r\n' % data)

        for c in self.factory.clients:
            if c is not self:
                c.sendLine(data)


class MyFactory(protocol.ServerFactory):
    protocol = MyProtocol

    def startFactory(self):
        self.clients = []

reactor.listenTCP(8000, MyFactory())
reactor.run()
