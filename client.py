__author__ = 'ka_python'
from twisted.internet import reactor, protocol

class Client(protocol.Protocol):
    def dataReceived(self, data):
        print(data)

    def send_message(self, data):
        self.transport.write('%s\r\n' % data)

def add_message(user):
    user.send_message('Wazzap bro')

user = protocol.ClientCreator(reactor, Client)
user.connectTCP('localhost', 8000).addCallback(add_message)

reactor.run()
